### About ###
This repo is to install open edx ginko version

### After ssh into the server execute the scripts ###
1. sudo locale-gen en_US en_US.UTF-8
2. sudo dpkg-reconfigure locales
3. sudo apt-get update -y
4. sudo apt-get upgrade -y
5. sudo reboot
### Then, after your server comes back .... ###
1. cd ~
2. sudo wget https://bitbucket.org/agwinthant/edx-ginko-installation/raw/ce288fff504115de0d018e10db6933b2a32368ff/edx.install.sh
3. sudo chmod 755 edx.install.sh
4. sudo nohup ./edx.install.sh &

### For more information check this link ###
*** I have change some of his code to be compactable with ginko version ***

https://blog.lawrencemcdaniel.com/open-edx-installation/
